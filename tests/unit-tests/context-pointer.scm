;; guile-cairo unit test
;; Copyright (C) 2023  David Pirotte <david@altosw.be>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(use-modules (unit-test)
             (oop goops)
             (cairo))

(define-class <test-context-pointer> (<test-case>))

(define-method (test-context-pointer (self <test-context-pointer>))
  (let* ((cs (cairo-image-surface-create 'argb32 140 100))
         (cr (cairo-create cs)))
    (assert (cairo-context->pointer cr))
    (assert (cairo-pointer->context (cairo-context->pointer cr)))))

(exit-with-summary (run-all-defined-test-cases))
